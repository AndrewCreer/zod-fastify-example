import fastify from 'fastify'
import FastifySwagger from '@fastify/swagger'

import autoLoad from '@fastify/autoload'
import {join} from 'path'
import {writeFileSync} from 'fs'

/**
 * NOTE Sun 22 May 2022 22:06:39 AEST
 * the swagger stuff fails on v4 versions.
 * I guess that are not 'released' yet
 
npm info fastify |grep dist-tags -A2
dist-tags:
latest: 3.29.0
next: 4.0.0-rc.2

npm info @fastify/swagger |grep dist-tags -A2
dist-tags:
latest: 6.0.1
next: 7.0.0

 */

// Basic config for top level swagger
const server = fastify({logger: true})
server.register(FastifySwagger, {
  routePrefix: '_docs',
  exposeRoute: true,
  // Use swagger:{} for swaggerv2
  openapi: {
    info: {
      title: 'coffee',
      description: 'roast logger',
      version: '1'
    }
  }
})

// Use https://github.com/fastify/fastify-autoload to autoload this this file..
// NOTE from DOCS
// > Each script file within a directory is treated as a plugin unless the
// > directory contains an index file (e.g. index.js). In that case only the
// > index file (and the potential sub-directories) will be loaded.
// Various options specify naming convention for which files are to be loaded or not
server.register(autoLoad, {
  dir: join(__dirname, 'routes'),
  dirNameRoutePrefix: false // Allow the routes to set their own URLs. We dont want plugin adding in extra baseUrls
})

server.listen({port: 8000}, err => {
  if (err) {
    server.log.error(err)
    process.exit(1)
  }
})

// TODO Not in prod!
server.ready(() => {
  writeFileSync('./docs/autogenerated-swagger.json', JSON.stringify(server.swagger(), null, 1))
})
