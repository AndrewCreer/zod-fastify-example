import z from 'zod'

// When we  create (post) we have some shape rules, regex, length, enum type
// things.  the name has lowerCaseFirstLetter (as a convention?)
export const createGoose = z.object({
  name: z.string().min(5).max(30).regex(/^G/),
  number: z.number(),
  email: z.string().email()
})
// And the type with the same name but UpperCaseFirstLetter (again as a convention)...
export type CreateGoose = z.infer<typeof createGoose>

// This describes the create version AFTER it comes back from a downstream
// (database, api, etc...) with an ID that was created by that system
export const gooseId = z.string().uuid()
export const goose = createGoose.extend({
  id: gooseId
})
export type Goose = z.infer<typeof goose>
