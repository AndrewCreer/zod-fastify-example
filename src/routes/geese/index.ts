import z from 'zod'
import type {FastifyPluginAsync, RouteOptions} from 'fastify'
import zodToJsonSchema from 'zod-to-json-schema'
import {v4} from 'uuid'

// Basic Structure of this file copied from
// https://github.com/fastify/fastify-cli/blob/master/templates/app-ts/src/routes/example/index.ts
// Just needs a default export the is a FastifyPluginAsync

// TODO Move all the 'non-fastify' goose business rule stuff to a './services'
// file.
// Robust permanent storage NOT!
const reliableGooseStorage: Goose[] = []

// Base URL for endpoints in this file..
const url = '/geese'

// Log the registration? See something on startup?
// TODO create a plugin that uses fastify hooks to do this for each 'register'
// call
const ROUTE_REG = 'ROUTE_REG'

/*
 * NOTE it is POSSIBLE to
 * https://www.fastify.io/docs/latest/Reference/Server/#addschema and put all
 * your schemas into 'fastify().addschema()' and to refer to them in routes
 * https://www.fastify.io/docs/latest/Reference/Routes/#routes-options
 * IF you want it obvious in your swagger that various fragments are identical
 *
 * Nothing here stops each endpoint doing its own addSchema and referring to them.
 */

import {goose, Goose, createGoose, CreateGoose, gooseId} from './schemas'

// Verbosely, set  up the POST endpoint
const postGoose: FastifyPluginAsync = async (server, options) => {
  // https://www.fastify.io/docs/latest/Reference/Routes/#routes-options
  const optionsToLogRegister: Pick<RouteOptions, 'url' | 'method'> = {
    url,
    method: 'POST'
  }

  server.log.info({
    message: ROUTE_REG,
    ...optionsToLogRegister,
    options // passed in by the AutoLoader
  })

  // https://www.fastify.io/docs/latest/Reference/TypeScript/#using-generics
  // search for RouteGenericInterface
  // You'd inline this.
  type GenericsProvideTypeSafteyInHander = {
    Body: CreateGoose
    Reply: Goose
    // Querystring: PossibeQuery
    // Params: PossibleParams
    // Headers: PossibleHeader
  }

  server.route<GenericsProvideTypeSafteyInHander>({
    ...optionsToLogRegister,
    schema: {
      body: zodToJsonSchema(createGoose, {name: 'CreateGoose'}).definitions.CreateGoose,
      response: {
        200: zodToJsonSchema(goose, {name: 'Goose'}).definitions.Goose
      }
    },
    handler: async (request, reply) => {
      request.log.info(`Hi from /geese. creating  ${request.body.name}`)
      // https://github.com/fastify/fastify-response-validation is available if you WANT to validate responses
      // OR just use the body
      const newOne = {
        ...request.body,
        id: v4()
      }

      // Some business rule.. Note intellisense on request.body.
      if (reliableGooseStorage.find(({number}) => number === request.body.number)) {
        // TODO make this a 400 bad data, rather than the default 500
        throw new Error(`You have already made Goose number ${request.body.number} `)
      }

      reliableGooseStorage.push(newOne)
      reply.send(newOne)
    }
  })
}

// Succintly define the GetAll endpoint
const getAllGeese: FastifyPluginAsync = async server => {
  server.log.info({message: ROUTE_REG, url, method: 'get'})
  server.get<{Reply: Goose[]}>(url, {
    schema: {
      response: {
        200: zodToJsonSchema(z.array(goose), {name: 'Gooses'}).definitions.Gooses
      }
    },
    handler: async (_request, reply) => {
      reply.send(reliableGooseStorage)
    }
  })
}

// Get a particular goose.
const getOneGoose: FastifyPluginAsync = async server => {
  const myurl = `${url}/:gooseId`
  server.log.info({message: ROUTE_REG, url: myurl, method: 'get'})
  server.get<{Reply: Goose; Params: {gooseId: z.infer<typeof gooseId>}}>(myurl, {
    schema: {
      response: {200: zodToJsonSchema(goose, {name: 'Goose'}).definitions.Goose}
    },
    handler: async (request, reply) => {
      const matching = reliableGooseStorage.filter(({id}) => id === request.params.gooseId)
      request.log.info({matching})
      switch (matching.length) {
        case 1:
          reply.send(matching.at(0))
          break
        case 0:
          throw new Error(`Goose ${request.params.gooseId} not found`)
        default:
          throw new Error('whatever')
      }
    }
  })
}

// TODO If this file is has a tooManyLines code smell, move each endpoint into
// its own file?  or Move all business logic into a './business-logic.ts' file
const geese: FastifyPluginAsync = async (server, options) => {
  await getOneGoose(server, options)
  await getAllGeese(server, options)
  await postGoose(server, options)
}

// Autoload plugin uses default export.
export default geese
